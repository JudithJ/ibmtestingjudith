package Activity_practice;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterClass;

public class Activity4_5 {
	WebDriver driver;
  @Test
  public void f() {
	  WebElement navi;
		navi = driver.findElement(By.xpath("/html/body/div/header/div/div/div/div/div[3]/div/nav/div/ul/li[5]/a"));
		String navig = navi.getText();
		System.out.println("Menu is :" + navig);
		navi.click();
		String title = driver.getTitle();
		System.out.println("Title is : " + title);
		
  }
  
@BeforeClass
  public void beforeClass() {
	  driver = new FirefoxDriver();
	  driver.get("https://alchemy.hguy.co/lms");
  }

  @AfterClass
  public void afterClass() {
	  driver.close();
  }

}
