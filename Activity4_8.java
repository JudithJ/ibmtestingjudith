package Activity_practice;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterClass;

public class Activity4_8 {
	WebDriver driver;

	@Test
  public void f() {
	  WebElement contact;
	  WebElement fullname;
	  WebElement email;
	  WebElement subj;
	  WebElement message;
	  WebElement submit;
	  WebElement thanku;
	  contact = driver.findElement(By.linkText("Contact"));
	  contact.click();
	  fullname = driver.findElement(By.id("wpforms-8-field_0"));
	  fullname.sendKeys("Judith Dorathy");
	  email = driver.findElement(By.id("wpforms-8-field_1"));
	  email.sendKeys("Judith@email.com");
	  subj = driver.findElement(By.id("wpforms-8-field_3"));
	  subj.sendKeys("Selenium");
	  message = driver.findElement(By.id("wpforms-8-field_2"));
	  message.sendKeys("Selenium");
	  submit = driver.findElement(By.id("wpforms-submit-8"));
	  submit.click();
	  thanku = driver.findElement(By.xpath("/html/body/div[1]/div/div/div/main/article/div/section[4]/div[2]/div[2]/div[2]/div[2]/p"));
	  String tq = thanku.getText();
	  System.out.println("The message displayed after submission is : " + tq);
	  
			  
  }

	@BeforeClass
	public void beforeClass() {
		driver = new FirefoxDriver();
		driver.get("https://alchemy.hguy.co/lms");

	}

	@AfterClass
	public void afterClass() {
		driver.close();
	}

}
