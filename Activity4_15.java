package Activity_practice;

import org.testng.annotations.Test;

import org.testng.annotations.BeforeClass;
import org.apache.commons.io.FileUtils;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;

public class Activity4_15 {
	static WebDriver driver;

	@Test
	public void f() throws InterruptedException, Exception {

		WebElement linkt;
		linkt = driver.findElement(By.linkText("All Courses"));
		linkt.sendKeys(Keys.ENTER);
		List<WebElement> courses = driver.findElements(By.className("entry-title"));
		Reporter.log("Number of Courses are : " + courses.size());
		Reporter.log("Selecting the First course");
		WebElement seemore;
		seemore = driver.findElement(By.xpath("//a[@class='btn btn-primary']"));
		seemore.sendKeys(Keys.ENTER);
		JavascriptExecutor js = ((JavascriptExecutor) driver);
		js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
		WebElement topic;
		topic=driver.findElement(By.xpath("//span[@class='ld-icon-arrow-down ld-icon']"));
		topic.click();
		
		JavascriptExecutor js1 = ((JavascriptExecutor) driver);
		js1.executeScript("window.scrollTo(0, document.body.scrollHeight)");
		//driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		Thread.sleep(5000);
		
		WebElement les;
		les = driver.findElement(By.xpath("//a[@class='ld-table-list-item-preview ld-primary-color-hover ld-topic-row learndash-incomplete ']"));
		boolean sel = les.isEnabled();
		System.out.println(sel);
		les.sendKeys(Keys.ENTER);
		
		JavascriptExecutor js2 = ((JavascriptExecutor) driver);
		js2.executeScript("window.scrollTo(0, document.body.scrollHeight)");
		Thread.sleep(5000);
				
		WebElement markc;
		markc = driver.findElement(By.xpath("//input[@type='submit']"));
		markc.click();
		
					
		js1.executeScript("window.scrollTo(0, document.body.scrollHeight)");
		
		Thread.sleep(5000);
		
		WebElement prog;
		prog = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[1]/div[3]/div/div[1]/div/div[1]"));
		Reporter.log("The progress is :" +prog.getText());
		
		
		
	 TakesScreenshot scrShot =((TakesScreenshot)driver);
      String timeStamp;
      timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime());

        //Call getScreenshotAs method to create image file

                File SrcFile=scrShot.getScreenshotAs(OutputType.FILE);

            //Move image file to new destination

                File DestFile=new File("src/test/resources/"+timeStamp+".jpg");

                //Copy file at destination

                FileUtils.copyFile(SrcFile, DestFile);	
		
		
	    				
	}

	@BeforeClass
	public void beforeClass() {
		driver = new FirefoxDriver();
		driver.get("https://alchemy.hguy.co/lms");
		Reporter.log("Browser is open");
		
		WebElement navi;
		WebElement button;
		WebElement user;
		WebElement pass;
		WebElement login;
		WebElement nameroot;
		navi = driver.findElement(By.xpath("/html/body/div/header/div/div/div/div/div[3]/div/nav/div/ul/li[5]/a"));
		String navig = navi.getText();
		Reporter.log("Menu is :" + navig);
		navi.click();
		String title = driver.getTitle();
		Reporter.log("Title is : " + title);
		
		button = driver.findElement(By.xpath("/html/body/div[1]/div/div/div/main/article/div/section[2]/div[2]/div[2]/div[2]/div[2]/a"));
		button.click();
		
		user = driver.findElement(By.id("user_login"));
		pass = driver.findElement(By.id("user_pass"));
		user.sendKeys("root");
		pass.sendKeys("pa$$w0rd");
		
		login = driver.findElement(By.id("wp-submit"));
		login.click();
		
		nameroot = driver.findElement(By.className("display-name"));
		String name = nameroot.getText();
		Reporter.log("Login display Name : " + name);
	
	}

	
	@AfterClass
	public void afterClass() {
		driver.close();
		Reporter.log("Test Completed");
	}
}
