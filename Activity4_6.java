package Activity_practice;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterClass;

public class Activity4_6 {
	WebDriver driver;

	@Test
	public void f() {
		WebElement navi;
		WebElement button;
		WebElement user;
		WebElement pass;
		WebElement login;
		WebElement nameroot;
		navi = driver.findElement(By.xpath("/html/body/div/header/div/div/div/div/div[3]/div/nav/div/ul/li[5]/a"));
		String navig = navi.getText();
		System.out.println("Menu is :" + navig);
		navi.click();
		String title = driver.getTitle();
		System.out.println("Title is : " + title);
		
		button = driver.findElement(By.xpath("/html/body/div[1]/div/div/div/main/article/div/section[2]/div[2]/div[2]/div[2]/div[2]/a"));
		button.click();
		
		user = driver.findElement(By.id("user_login"));
		pass = driver.findElement(By.id("user_pass"));
		user.sendKeys("root");
		pass.sendKeys("pa$$w0rd");
		
		login = driver.findElement(By.id("wp-submit"));
		login.click();
		
		nameroot = driver.findElement(By.className("display-name"));
		String name = nameroot.getText();
		System.out.println("Login display Name : " + name);
				

	}

	@BeforeClass
	public void beforeClass() {
		driver = new FirefoxDriver();
		driver.get("https://alchemy.hguy.co/lms");
	}

	@AfterClass
	public void afterClass() {
		driver.close();
	}

}
