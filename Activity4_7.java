package Activity_practice;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterClass;

public class Activity4_7 {
	WebDriver driver;
	@Test
	public void f() {
		
		WebElement linkt;
		linkt = driver.findElement(By.linkText("All Courses"));
		linkt.click();
		List<WebElement> courses = driver.findElements(By.className("entry-title"));
		System.out.println("Number of Courses are : " + courses.size());
	}

	@BeforeClass
	public void beforeClass() {
		driver = new FirefoxDriver();
		driver.get("https://alchemy.hguy.co/lms");
	}

	@AfterClass
	public void afterClass() {
		driver.close();
	}

}
