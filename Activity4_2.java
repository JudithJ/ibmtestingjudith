package Activity_practice;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;

public class Activity4_2 {
	WebDriver driver;
	//WebDriverWait wait = new WebDriverWait(driver, 10);

  @Test
  public void f() {
	  WebElement heading;
		heading = driver.findElement(By.xpath("//h1[@class='uagb-ifb-title']"));
		String headh = heading.getText();
		System.out.println("Page Heading is :" + heading.getText());
		Assert.assertEquals("Learn from Industry Experts", headh);
  }
  @BeforeClass
  public void beforeClass() {
	  			driver = new FirefoxDriver();
	  			driver.get("https://alchemy.hguy.co/lms");
  }

  @AfterClass
  public void afterClass()  {
	// Close the browser
	 driver.quit();
  }

}
