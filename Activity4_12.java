package Activity_practice;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterClass;

public class Activity4_12 {
	WebDriver driver;

	@Test
	public void f() throws InterruptedException {

		WebElement linkt;
		linkt = driver.findElement(By.linkText("All Courses"));
		linkt.click();
		List<WebElement> courses = driver.findElements(By.className("entry-title"));
		System.out.println("Number of Courses are : " + courses.size());
		System.out.println("Selecting the First course");
		WebElement seemore;
		seemore = driver.findElement(By.xpath("//a[@class='btn btn-primary']"));
		seemore.click();
		JavascriptExecutor js = ((JavascriptExecutor) driver);
		js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
		WebElement topic;
		topic=driver.findElement(By.xpath("//span[@class='ld-icon-arrow-down ld-icon']"));
		topic.click();
		
		JavascriptExecutor js1 = ((JavascriptExecutor) driver);
		js1.executeScript("window.scrollTo(0, document.body.scrollHeight)");
		//driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		Thread.sleep(5000);
		
		WebElement les;
		les = driver.findElement(By.xpath("//a[@class='ld-table-list-item-preview ld-primary-color-hover ld-topic-row learndash-incomplete ']"));
		boolean sel = les.isEnabled();
		System.out.println(sel);
		les.click();
		
		JavascriptExecutor js2 = ((JavascriptExecutor) driver);
		js2.executeScript("window.scrollTo(0, document.body.scrollHeight)");
		
				
		WebElement markc;
		markc = driver.findElement(By.xpath("//input[@type='submit']"));
		markc.click();
		
					
		js1.executeScript("window.scrollTo(0, document.body.scrollHeight)");
		
		Thread.sleep(5000);
		
		WebElement prog;
		prog = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[1]/div[3]/div/div[1]/div/div[1]"));
		System.out.println("The progress is :" +prog.getText());
		
		
		
	    				
	}

	@BeforeClass
	public void beforeClass() {
		driver = new FirefoxDriver();
		driver.get("https://alchemy.hguy.co/lms");
		
		WebElement navi;
		WebElement button;
		WebElement user;
		WebElement pass;
		WebElement login;
		WebElement nameroot;
		navi = driver.findElement(By.xpath("/html/body/div/header/div/div/div/div/div[3]/div/nav/div/ul/li[5]/a"));
		String navig = navi.getText();
		System.out.println("Menu is :" + navig);
		navi.click();
		String title = driver.getTitle();
		System.out.println("Title is : " + title);
		
		button = driver.findElement(By.xpath("/html/body/div[1]/div/div/div/main/article/div/section[2]/div[2]/div[2]/div[2]/div[2]/a"));
		button.click();
		
		user = driver.findElement(By.id("user_login"));
		pass = driver.findElement(By.id("user_pass"));
		user.sendKeys("root");
		pass.sendKeys("pa$$w0rd");
		
		login = driver.findElement(By.id("wp-submit"));
		login.click();
		
		nameroot = driver.findElement(By.className("display-name"));
		String name = nameroot.getText();
		System.out.println("Login display Name : " + name);
	}

	@AfterClass
	public void afterClass() {
		driver.close();
	}

}
