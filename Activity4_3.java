package Activity_practice;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;

public class Activity4_3 {
	WebDriver driver;

	@Test
	public void f() {
		WebElement infohead;
		infohead = driver.findElement(By.xpath("//h3[@class='uagb-ifb-title']"));
		String headh = infohead.getText();
		System.out.println("Page Heading is :" + headh);
		Assert.assertEquals("Actionable Training", headh);

	}

	@BeforeClass
	public void beforeClass() {

		driver = new FirefoxDriver();
		driver.get("https://alchemy.hguy.co/lms");
	}

	@AfterClass
	public void afterClass() throws InterruptedException {
		// Close the browser
		Thread.sleep(1000);
		driver.quit();
	}

}
